var api=require('../config/api')
// 封装
function request(url,data={},method='GET'){
  return new Promise(function(resolve,reject){
    wx.request({
      url:url,
      data:data,
      method:method,
      header:{
        'Content-Type': 'application/json',
      },
      success:function(res){
        console.log('success');
        if(res.statusCode>=200&&res.statusCode<300||res.statusCode===304){
          if(res.data.erron==401){

          }else{
            resolve(res.data)
          }
        }else{
          reject(res.errMsg)
        }
      }
    })
  })
}

module.exports={
  request
}