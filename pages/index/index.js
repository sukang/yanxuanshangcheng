//index.js
const api=require('../../config/api')
const until=require('../../requestf/requestf')
import {getindex} from '../../http/apis'
//获取应用实例
const app = getApp()

Page({
  onShareAppMessage() {
    return {
      title: 'swiper',
      path: 'page/component/pages/swiper/swiper'
    }
  },

  data: {
   banner:[],
   channel:[],
   newGoodsList:[],
   hotGoodsList:[],
   brandList:[],
   topicList:[],
   categoryList:[]
  },
  getData:function(){
    var that=this;
    until.request(api.IndexUrl).then(function(res){
      console.log(res.data.banner)
      that.setData({
        banner:res.data.banner,
        channel:res.data.channel,
        newGoodsList:res.data.newGoodsList,
        hotGoodsList:res.data.hotGoodsList,
        brandList:res.data.brandList,
        topicList:res.data.topicList,
        categoryList:res.data.categoryList
       })
    })
  },
 

  //事件处理函数
  bindViewTap: function() {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
   this.getData()
   getindex()
  }

})

