// pages/special/specail.js
const api=require('../../config/api')
const until=require('../../requestf/requestf')
import {getspacil} from '../../http/apis'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    special:[]
  },
  getData:function(){
    var that=this;
    until.request(api.TopicList).then(function(res){
      console.log(res.data.data)
      that.setData({
        special:res.data.data
       })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getData()
    getspacil()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})