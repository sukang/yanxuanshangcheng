const baseUrl='http://182.92.211.236:8360/api/';
export const http =(url,data={},method='GET')=>{
  return new Promise((resolve,reject)=>{
    wx.request({
      url: baseUrl+url,
      method,
      data,
     success(res){
      console.log(res)
      if(res.statusCode>=200&&res.statusCode<300||res.statusCode===304){
        if(res.data.errno===0){
          resolve(res.data)
          }else{
            wx.showToast({
              title: res.data.errmsg,
              icon:'none' 
            })
            reject(res)
          }
      }else{
        wx.showToast({
          title: res.errMsg,
          icon:'none'
        })
      }
     },
     fail(err){
       wx.showToast({
         title: err,
         icon:'none'
       })
       reject(err)
     }
    })
  })
}
