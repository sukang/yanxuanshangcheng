import {http} from '../http/http'

// 首页
export function getindex(){
  return http('catalog/index').then(res=>{
    return res.data
  })
}
// 专题
export function getspacil(){
  return http('TopicList').then(res=>{
   
    return res.data
  })
}
// 分类
export function getclass(){
  return http('catalog/index').then(res=>{
    return res.data
  })
}
// 购物车
export function getshopping(){
  return http('cart/index').then(res=>{
    return res.data
  })
}
// 我的
export function getMyinfo(){
  return http('catalog/index').then(res=>{
    return res.data
  })
}